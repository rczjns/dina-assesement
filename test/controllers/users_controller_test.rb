require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  test 'user list' do
    get :index
    assert_response :success
  end

  test 'create user' do
    post :create, user: { name: 'new user name', status: ApplicationHelper::Constants::STATUS_OPTIONS.first }

    assert_redirected_to users_path
    assert_equal ApplicationHelper::Constants::SUCCESS_NOTICE, flash[:notice]
  end

  test 'create user without name' do
    post :create, user: { status: ApplicationHelper::Constants::STATUS_OPTIONS.first }

    assert_response :success
    assert_nil flash[:notice]
  end

  test 'modify' do
    post :create, user: { name: 'new user name' }
    post :update, id: 1, user: { name: 'modified user name' }

    assert_redirected_to users_path
    assert_equal ApplicationHelper::Constants::SUCCESS_NOTICE, flash[:notice]
  end
end
