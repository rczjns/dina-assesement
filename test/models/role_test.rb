require 'test_helper'

class RoleTest < ActiveSupport::TestCase
  test 'create role with permission' do
    role = Role.create(name: 'My role')
    role.permissions.create(name: 'My permission')

    role_from_db = Role.first
    assert_equal 'My role', role_from_db.name
    assert_equal 'My permission', role_from_db.permissions.first.name
  end

  test 'start with no match' do
    Role.create(name: 'Role')

    roles_start_a = Role.starts_with('a')
    assert_equal 0, roles_start_a.size
  end

  test 'start with match' do
    Role.create(name: 'Role 1')
    Role.create(name: 'Role 2')

    roles_start_r = Role.starts_with('R')
    assert_equal 2, roles_start_r.size
  end
end
