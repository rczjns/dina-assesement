require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test 'create new user with full data' do
    role = Role.create(name: 'New role')
    role.permissions.create(name: 'New role permission')
    user = User.create(name: 'Name', status: 'My status', role: role)
    user.permissions.create(name: 'New permission')

    user_from_db = User.first

    assert_equal 'Name', user_from_db.name
    assert_equal 'New permission', user_from_db.permissions.first.name
    assert_equal 'New role', user_from_db.role.name
    assert_equal 'New role permission', user_from_db.role.permissions.first.name
  end

  test 'new user without name' do
    user = User.new
    refute user.save
  end

  test 'user with no permissions' do
    User.create(name: 'Name')

    user_from_db = User.first

    assert 0, user_from_db.all_permissions
  end

  test 'all permissions' do
    permission = Permission.create(name: 'Permission 1')
    role = Role.create(name: 'Role')
    role.permissions << permission
    user = User.create(name: 'Name', role: role)
    user.permissions << permission

    user_from_db = User.first

    assert 1, user_from_db.all_permissions
  end
end
