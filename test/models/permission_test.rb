require 'test_helper'

class PermissionTest < ActiveSupport::TestCase
  test 'create permission' do
    Permission.create(name: 'new permission')
    permissions = Permission.all

    assert_equal 1, permissions.size
  end
end
