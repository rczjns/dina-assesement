class CreateRoles < ActiveRecord::Migration
  def change
    create_table :roles do |t|
      t.string :name

      t.timestamps null: false
    end

    create_table :permissions_roles, id: false do |t|
      t.belongs_to :permission, index: true
      t.belongs_to :role, index: true
    end
  end
end
