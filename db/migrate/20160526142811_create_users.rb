class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :status
      t.references :role, index: true, foreign_key: true

      t.timestamps null: false
    end

    create_table :permissions_users, id: false do |t|
      t.belongs_to :user, index: true
      t.belongs_to :permission, index: true
    end
  end
end
