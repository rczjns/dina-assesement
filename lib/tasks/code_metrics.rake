require 'rubocop/rake_task'

namespace :code_metrics do
  desc 'Execute RuboCop'
  RuboCop::RakeTask.new(:rubocop) do |t|
    t.options = %w(-foffenses -fprogress -olog/rubocop.log)
    t.fail_on_error = false
  end

  desc 'Run all analysis'
  task run_all: [:clean, :rubocop] do
  end
end
