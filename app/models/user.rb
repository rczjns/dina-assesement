class User < ActiveRecord::Base
  belongs_to :role
  has_and_belongs_to_many :permissions
  validates :name, presence: true

  def all_permissions
    perms = Set.new(permissions)
    perms.merge(role.permissions) if role
    perms
  end
end
