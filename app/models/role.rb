class Role < ActiveRecord::Base
  has_and_belongs_to_many :permissions

  scope :starts_with, ->(start) { where('name LIKE ?', "#{start}%") }
end
