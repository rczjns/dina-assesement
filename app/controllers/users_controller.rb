class UsersController < ApplicationController
  def index
    render locals: local_parameters
  end

  def create
    actual_user = User.new(user_params)

    respond_to do |format|
      if actual_user.save
        format.html { redirect_to users_path, notice: ApplicationHelper::Constants::SUCCESS_NOTICE }
        format.js   do
          render locals: { success: true,
                           notice: ApplicationHelper::Constants::SUCCESS_NOTICE,
                           saved_user: actual_user }.merge!(local_parameters)
        end
        format.json { render json: actual_user, status: :created }
      else
        format.html { render 'index', locals: local_parameters(actual_user) }
        format.js   { render locals: { success: false }.merge!(local_parameters(actual_user)) }
        format.json { render json: actual_user.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    user = User.find(params[:id])
    if user.update(user_params)
      redirect_to users_url
    else
      render 'index', locals: local_parameters(user)
    end
  end

  def edit
    user = User.find(params[:id])
    render 'index', locals: local_parameters(user)
  end

  private

  def local_parameters(actual_user = nil)
    { user: actual_user.nil? ? User.new : actual_user,
      all_user: User.all,
      roles: Role.all }
  end

  def user_params
    params.require(:user).permit(:name, :status, :role_id)
  end
end
