module ApplicationHelper
  module Constants
    SUCCESS_NOTICE = 'User was successfully created.'.freeze
    STATUS_OPTIONS = ['Status 1', 'Status 2'].freeze
  end
end
